object Main extends App {
 
  // zadanie 1
  def matchDay(day: String): String = day match {
    case "Monday" | "Tuesday" | "Wednesday" | "Thursday" |  "Friday" => "Praca"
    case "Saturday" | "Sunday" => "Weekend"
    case _ => "No such day"
  }
  //sprawdzenie zadania 1
  println(matchDay("Monday"))
  println(matchDay("Sunday"))
  println(matchDay("abrakadabra"))

  // zadanie 2
  class KontoBankowe() {
    private var _stanKonta = 0;
    def this (stanKonta: Int) {
      this();
      this._stanKonta = stanKonta;
    }
    def stanKonta = _stanKonta;
    def wplata(value: Int): Unit = {
      _stanKonta += value;
    }
    def wyplata(value: Int): Unit = {
      _stanKonta -= value;
    }
  }
  // sprawdzenie zadania2
  var konto = new KontoBankowe()
  println(konto.stanKonta)
  konto.wplata(30)
  konto.wyplata(20)
  println(konto.stanKonta)
  var konto2 = new KontoBankowe(200);
  println(konto2.stanKonta)

  // zadanie3
  case class Osoba(val imie: String, val nazwisko: String){}
  
  def przywitaj(osoba: Osoba): String = osoba match {
    case Osoba(imie, nazwisko) if imie.contains("Paweł") && nazwisko.contains("Wąsowicz") => "Witaj Pawle stary druchu"
    case Osoba(imie, nazwisko) if imie.contains("Michał") => "Witaj Michale"
    case default => "Witaj nieznajomy"
  }
  // sprawdzenie zadania3
  val pawel = new Osoba("Paweł", "Wąsowicz")
  val inny_pawel = new Osoba("Paweł", "Kukiz")
  val michal = new Osoba("Michał", "Zakościelny")
  val robert =  new Osoba("Robert", "Kubica")

  println(przywitaj(pawel))
  println(przywitaj(inny_pawel))
  println(przywitaj(michal))
  println(przywitaj(robert))

  // zadanie 4
  def threeOperation(number: Int, f: (Int) => Int): Int = {
    f(f(f(number)))
  }
  def addone(number: Int): Int = {number +1}
  // sprawdzenie zadania4
  println(threeOperation(5, addone))

  // zadanie 5
  class Person(){
    protected var _imie = ""
    protected var _nazwisko = ""
    protected var _podatek: Double = 0
    protected var _scale: Double = 0
  }

  trait Pracownik extends Person {
    protected var _pensja = 0
    _scale = 0.20
    def setImieNazwisko(imie: String, nazwisko: String){
      _imie = imie
      _nazwisko = nazwisko
    }
    def getImie(): String ={
      return _imie
    }

    def getNazwisko(): String = {
      return _nazwisko
    }
    
    def setPensja(pensja: Int){
      _pensja = pensja
      _podatek = pensja * _scale
    }
    def getPensja(): Double = {
      return _pensja
    }
    def getPodatek(): Double = {
      return _podatek
    }
  }

  trait Student extends Pracownik {
    _scale = 0
  }

  trait Nauczyciel extends Pracownik {
    _scale = 0.1
  }

  
  val pracownik = new Pracownik{}
  pracownik.setPensja(1000)
  println(pracownik.getPensja())
  println(pracownik.getPodatek())

  val student = new Student{}
  student.setPensja(800)
  println(student.getPensja())
  println(student.getPodatek())

}